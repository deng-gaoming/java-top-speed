package chapter04;

public class Java02_Object_2 {
    public static void main(String[] args) {
        Teacher teacher = new Teacher();
        teacher.name = "zhangsan";
        teacher.teach();
        Student student = new Student();
        student.name = "lisi";
        student.study();
    }
}
class Teacher{
    String name;
    void teach(){
        System.out.println(name+"在讲课");
    }
}

class Student{
    String name;
    void study(){
        System.out.println(name+"在听讲");
    }
}
