package chapter04;

public class Java01_Object {
    public static void main(String[] args) {
        // TODO 面向对象
        // 所谓的面向对象,其实是分析问题时，以问题所涉及到的事或物为中心的分析方式
        Cooking c = new Cooking();
        c.name="红烧排骨";
        c.food="排骨";
        c.execute();

        Cooking c1 = new Cooking();
        c1.name = "红烧鱼";
        c1.food = "鲫鱼";
        c1.execute();
    }
}

class Cooking{
    String name;
    String type = "红烧";
    String food;
    String relish = "大料";

    void execute(){
        System.out.println("准备食材"+food);
        System.out.println("准备佐料"+relish);
        System.out.println("开始烹饪");
        System.out.println(name+"烹饪完成");
    }

}
