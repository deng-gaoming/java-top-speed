package chapter04;

public class Java06_Object_Method {
    public static void main(String[] args) {
        User06 user = new User06();
        boolean registerResult = user.register();
        if(registerResult){
            System.out.println("注册成功");
            boolean loginResult = user.login();
            if(loginResult){
                System.out.println("登录成功");
            }else {
                System.out.println("登录失败");
            }

        }else {
            System.out.println("注册失败");
        }
    }
}

class User06{
    String account;
    String password;
    boolean register(){
        System.out.println("用户注册");
        return true;
    }
    boolean login(){
        System.out.println("用户登录");
        return false;
    }
}
