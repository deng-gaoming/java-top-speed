package chapter04;

public class Java07_Object_Method_Param {
    public static void main(String[] args) {
            User07 user = new User07();
            String name = "zhangsan";
            int age=30;

            user.sayHello(name,age);

            user.test("lisi","wangwu");
    }
}

class User07{
    void sayHello(String name,int age){
        System.out.println("Hello "+name+", "+age);
    }
    void test(String ...args){
        for(int i=0;i<args.length;i++){
            System.out.println(args[i]);
        }
    }
}
