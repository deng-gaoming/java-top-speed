package chapter02;

public class Java03_Datatype {
    public static void main(String[] args) {

        // 1.整数类型
        // byte 8位
        byte b=10;
        // short 16位
        short s = 10;
        // int 32位
        int i = 10;
        // long  64 位
        long lon = 10;

        // 2.浮点类型
        float f = 1.0F;
        double d = 2.0;

        // 3.字符类型
        char c = '@';

        // 4. 布尔类型
        boolean bln = true;





    }
}
