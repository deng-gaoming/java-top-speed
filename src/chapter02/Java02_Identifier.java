package chapter02;

public class Java02_Identifier {
    public static void main(String[] args) {
        String name="zhangsan";
        String username = "zhangsan";
        String xingming = "lisi";
        String _name="wangwu";
        String $name="wangwu";
        String $na_me="wangwu";
        System.out.println(_name);
        System.out.println($name);
        System.out.println($na_me);

//      String 1name = "zhangsan";
        String name1 = "zhangsan";
        String Name = "zhangsan";
// 可以使用大小写实现关键字定义，但是容易造成歧义，不推荐
        String Public = "zhaoliu";

        String userName = "zhangsan";

//        String 哈哈 = '123';

    }
}
